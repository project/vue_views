<?php

use Drupal\Core\Template\Attribute;

/**
 * @file
 * Theme for Vue views.
 */
function template_preprocess_vue_views_default(&$variables) {
  $view = $variables['view'];
  $options = $view->style_plugin->options;
  $display_format = $options['display_format'];
  //$rows = $variables['rows'];
  /* Add this view to drupal settings */
  $variables['view_id'] = $viewid = $view->Id();
  $variables['display_id'] = $displayid = $view->current_display;
  $short_cache  = \Drupal::service('vue_views.global_short_cache');
  $views = $short_cache->getData('views');
  $views[$viewid.'_'.$displayid] =
  $variables['#attached']['drupalSettings']['vueViews']['views'][] = [
    'view' => $viewid,
    'display' => $displayid,
    'display_format' => $display_format
  ];
  $short_cache->setData('views', $views);
}
