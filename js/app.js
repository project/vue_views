/**
 * @file
 * vueView js.
 */

(function (url, settings, store) {
  'use strict';
  // Allows to use t() function in templates
  Vue.directive('t', function (event) {
    event.innerHTML = Drupal.t(event.innerHTML.trim());
  });

/*
  let vue_views_test_page_1 = {
      template: '<div>\
      <h3>{{ view_title }}</h3>\
      <table class="admin-dblog-ui">\
        <thead>\
          <tr>\
            <th v-for="field in view.fields">\
                  {{ field }}\
            </th>\
          </tr>\
        </thead>\
          <transition name="dblog-ui">\
            <tbody :class="{\'dblog-ui-data-loading\' : loading}">\
              <tr v-for="row in view.results" :key="row.id">\
                <td v-for="value in row">\
                  {{ value }}\
                </td>\
              </tr>\
              <tr v-if="!view.results.length" class="dblog-ui-placeholder">\
                <td colspan="6" v-if="loading">Loading...</td>\
                <td colspan="6" v-else>No log messages available.</td>\
              </tr>\
            </tbody>\
          </transition>\
        </table>\
      </div>',
      data: function () {
        return {
          view: {
            results: [],
            fields: [],
          },
          loading: false,
          totalPages: 1,
          view_title: 'A Vue View'
        };
      },
  };
*/

  var view = settings.vueViews.views[0].view;
  var viewDisplay = settings.vueViews.views[0].display;
  var defaults = {
    mounted: function () {
      this.updateData();
    },
    watch: {
      $route: 'updateData'
    },
    methods: {
      updateData: function () {
        this.loading = true;
        var that = this;
        store.getRecords(view, viewDisplay, this.$route.query, function (data) {
          that.view.results = data.data;
          that.view.fields = data.fields;
          that.loading = false;
        });
      },
      activeClass: function (order) {
        return this.$route.query.order === order ? 'is-active' : '';
      }
    }
  };

  var vue_views_def = view + '_' + viewDisplay;
  var customDef = {};
  eval('customDef='+vue_views_def);
  const componentDef = {...defaults, ...customDef};
  var vueViewList = Vue.component( 'vue-views-display-' + view + '-' + viewDisplay, componentDef );
  var router = new VueRouter({
    base: settings.path.baseUrl + settings.path.currentPath,
    routes: [
      { path: '/', component: vueViewList }
    ]
  })
  var app = new Vue({
    router: router,
    template: '<router-view></router-view>',
  }).$mount('#' + 'vue-views-app-' + view + '-' + viewDisplay);

} (Drupal.url, drupalSettings, new VueViewsStore()));
