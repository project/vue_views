/**
 * @file
 * Events store service.
 */

window.VueViewsStore = function () {

  'use strict';

  /* global Vue */

  var endPoint = 'api/vue-views/view-data/';

  function encodeQueryData(query) {
    var ret = [];
    for (var param in query) {
      ret.push(encodeURIComponent(param) + '=' + encodeURIComponent(query[param]));
    }
    return ret.join('&');
  }

  this.getRecords = function (view, display, query, callback) {
    var url = Drupal.url(endPoint + view + '/' + display + '?' + encodeQueryData(query));

    function successCallback(response) {
      response
        .json()
        .then(callback);
    }

    function errorCallback(response) {
      console.error(response);
    }

    Vue.http.get(url).then(successCallback, errorCallback);
  };

  /*this.getRecord = function (eventId, callback) {
    var url = Drupal.url(endPoint + 'event/' + eventId);

    function successCallback(response) {
      response
        .json()
        .then(callback);
    }

    function errorCallback(response) {
      console.error(response);
    }

    Vue.http.get(url).then(successCallback, errorCallback);
  };*/

};
