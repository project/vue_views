<?php

namespace Drupal\vue_views\Controller;

use Drupal\views\Views;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for DB log UI routes.
 */
class VueViewJsonData extends ControllerBase {

  /**
   * Constructs the controller object.
   */
  public function __construct() {

  }

  /**
   * list
   */
  public function viewDataJsonDisplay($viewname, $displayid) {
    $values = [];
    $view = views::getview($viewname);
    if ($view) {
      $view->setDisplay($displayid);
      $displayObj = $view->getDisplay();
      $view->execute();
      $fields = [];
      //echo "<pre>";
      foreach ($view->result as $rid => $row) {
        /* Analizando los tipos de field handler */
        /*$fldtmp = $view->field['field_listext'];
        $fldtmp_obj = $fldtmp->getItems($row)[0]['raw'];
        var_dump(get_class($fldtmp_obj));
        die();*/
        /**********************************************/
        //echo "=======================<br />";
        //echo "=======================<br />";
        //var_dump('rid:'.$rid);
        foreach ($view->field as $fid => $field ) {
          if (count($fields) != count($view->field)) {
            $fields[] = $fid;
          }
          //var_dump('fid:'.$fid);
          $plugin = null;
          if (method_exists($field, 'getItems')) {
            $fld_items = $field->getItems($row);
            if (count($fld_items)) {
              $plugin = $fld_items[0]['raw'];
            }
          }
          /** TODO: Revisar cada tipo de campo implementando su procesamiento.
           * Imagen: Drupal\image\Plugin\Field\FieldType\ImageItem
           * Title: Drupal\Core\Field\Plugin\Field\FieldType\StringItem
           * Body: Drupal\text\Plugin\Field\FieldType\TextWithSummaryItem
           * bool: Drupal\Core\Field\Plugin\Field\FieldType\BooleanItem
           * date: Drupal\datetime\Plugin\Field\FieldType\DateTimeItem
           * float: Drupal\Core\Field\Plugin\Field\FieldType\FloatItem
           * decimal: Drupal\Core\Field\Plugin\Field\FieldType\DecimalItem
           * integer: Drupal\Core\Field\Plugin\Field\FieldType\IntegerItem
           * link: Drupal\link\Plugin\Field\FieldType\LinkItem
           * list of integers:
           *           Drupal\options\Plugin\Field\FieldType\ListIntegerItem
           * list of floats:
           *           Drupal\options\Plugin\Field\FieldType\ListFloatItem
           * listo of texts:
           *           Drupal\options\Plugin\Field\FieldType\ListStringItem
           * mail: Drupal\Core\Field\Plugin\Field\FieldType\EmailItem
           * file reference: Drupal\file\Plugin\Field\FieldType\FileItem
           * content or taxonomy reference:
           *        Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem
           * formated text: Drupal\text\Plugin\Field\FieldType\TextItem
           * texto largo con formato:
           *         Drupal\text\Plugin\Field\FieldType\TextLongItem
           * texto con resumen:
           *         Drupal\text\Plugin\Field\FieldType\TextWithSummaryItem
           * texto normal:
           *         Drupal\Core\Field\Plugin\Field\FieldType\StringLongItem
           * texto sin formato:
           *         Drupal\Core\Field\Plugin\Field\FieldType\StringItem
          **/
          // Si es de tipo imagen
          switch(get_class($plugin)) {
            case 'Drupal\image\Plugin\Field\FieldType\ImageItem':
            case 'Drupal\file\Plugin\Field\FieldType\FileItem':
              $target_id = $field->getValue($row);
              $file = \Drupal\file\Entity\File::load($target_id);
              $value = file_create_url($file->getFileUri());
            break;
            default:
              $value = $field->getValue($row);
          }
          $values[$rid][$fid] = $value;
          // echo "**************************<br />";
        }
      }
    }
    $response = [
      'data' => $values,
      'fields' => $fields,
    ];
    return new JsonResponse($response);
  }

  /**
   * Formats a database log message.
   *
   * @param object $event
   *   The record from the watchdog table. The object properties are: wid, uid,
   *   severity, type, timestamp, message, variables, link, name.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The formatted log message.
   */
  public function formatMessage($event) {
    $message = Xss::filterAdmin($event->message);
    $variables = @unserialize($event->variables);
    // @codingStandardsIgnoreStart
    return $variables == NULL ? $message : t($message, $variables);
    // @codingStandardsIgnoreEn
  }

}
