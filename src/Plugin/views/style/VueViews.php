<?php

namespace Drupal\vue_views\Plugin\views\style;

use Drupal\core\form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render a list of years and months
 * in reverse chronological order linked to content.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "vue_views",
 *   title = @Translation("Vue Views display"),
 *   help = @Translation("Display vue using Vue js."),
 *   theme = "vue_views_default",
 *   display_types = { "normal" }
 * )
 */
class VueViews extends StylePluginBase {

  /**
   * Does this Style plugin allow Row plugins?
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the Style plugin support grouping of rows?
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;

  /**
   * Does the style plugin for itself support to add fields to it's output.
   *
   * This option only makes sense on style plugins without row plugins, like
   * for example table.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  /**
   * Default plugin display format
   *
   * @var string
   */
  protected $defaultDisplayFormat = 'table';

  /**
   * Available Display Formats
   *
   * @var string
   */
  protected $displayFormats = [];

  protected function getDisplayFormats() {
    return [
      'table' => t('table'),
      'grid' => t('Grid')
    ];
  }
  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['display_format'] = array('default' => $this->defaultDisplayFormat);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    // $options = $this->displayHandler->getFieldLabels(TRUE);
    $form['display_format'] = [
      '#type' => 'select',
      '#title' => t('Display Format'),
      '#default_value' => (isset($this->options['display_format'])) ?
        $this->options['display_format'] : $this->defaultDisplayFormat,
      '#options' => $this->getDisplayFormats(),
    ];
  }

}
