<?php

namespace Drupal\vue_views;

use Drupal\Core\Extension\ExtensionDiscovery;
use Drupal\Core\Extension\InfoParserInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Discovery service for front-end components provided by modules and themes.
 *
 * Components (anything whose info file 'type' is 'vue_views') are treated as Drupal
 * extensions unto themselves.
 */
class ComponentDiscovery extends ExtensionDiscovery implements ComponentDiscoveryInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The info parser.
   *
   * @var \Drupal\Core\Extension\InfoParserInterface
   */
  protected $infoParser;

  /**
   * ComponentDiscovery constructor.
   *
   * @param string $root
   *   The root directory of the Drupal installation.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\InfoParserInterface $info_parser
   *   The info parser.
   */
  public function __construct($root, ModuleHandlerInterface $module_handler, InfoParserInterface $info_parser) {
    parent::__construct($root);
    $this->moduleHandler = $module_handler;
    $this->infoParser = $info_parser;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponents() {
    // Find components.
    $components = $this->scan('vue_views');
    // Set defaults for module info.
    $defaults = array(
      'dependencies' => array(),
      'description' => '',
      'package' => 'Other',
      'version' => NULL,
    );

    // Read info files for each module.
    foreach ($components as $key => $component) {
      // Look for the info file.
      $component->info = $this->infoParser->parse($component->getPathname());
      // Merge in defaults and save.
      $components[$key]->info = $component->info + $defaults;
    }
    $this->moduleHandler->alter('component_info', $components);
    //var_dump($components);die();
    return $components;
  }

  public function getProcessedComponents() {
    $components = $this->getComponents();
    $views_definitions = $formats_definitions = [];
    foreach(array_reverse($components) as $key => $component) {
      $info = $this->infoParser->parse($component->getPathname());
      if (isset($info['formats'])) {
        $component_formats = $info['formats'];
        foreach($component_formats as $formatid => $format_def) {
          foreach($format_def as $attr => $val) {
            $formats_definitions[$formatid][$attr]
              = $component->subpath . '/' . $val;
              //=  $val;
          }
        }
      }
      if (isset($info['views'])) {
        $component_views = $info['views'];
        foreach($component_views as $viewid => $c_views) {
          foreach($c_views as $displayid => $view_def) {
            foreach($view_def as $attr => $val) {
              $views_definitions[$viewid . '_' . $displayid][$attr]
                = $component->subpath . '/' . $val;
                //=  $val;
            }
          }
        }
      }
    }
    $preprocessed = [
      'views' => $views_definitions,
      'formats' => $formats_definitions,
    ];
    return $preprocessed;
  }

}
