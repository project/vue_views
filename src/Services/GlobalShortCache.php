<?php

namespace Drupal\vue_views\Services;

/**
 * Class CustomMetatags.
 *
 * @package Drupal\losnahuales
 */
class GlobalShortCache {

  /**
   * Global Var Id.
   *
   */
  protected $id = 'vueViewsGlobals';

  public function setData($key, $value) {
    $current_values = isset($GLOBALS[$this->id])? $GLOBALS[$this->id] : [];
    $add = [$key=>$value];
    $GLOBALS[$this->id] = array_merge( $current_values, $add );
  }

  public function getData($key=null) {
    $current_values = isset($GLOBALS[$this->id])? $GLOBALS[$this->id] : [];

    if ($key!==null) {
      if (isset($current_values[$key])) {
        return $current_values[$key];
      } else {
        return null;
      }
    }
    return isset($GLOBALS[$this->id])? $GLOBALS[$this->id] : [];
  }

}
