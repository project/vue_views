import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'

Vue.use(VueRouter)

new Vue({
  el: '#vue-views-app',
  render: h => h(App)
})
