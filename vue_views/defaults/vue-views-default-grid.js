

let vue_views_test_page_1 = {
    template: '<div>\
    <h3>{{ view_title }}</h3>\
    GRID\
    </div>',
    data: function () {
      return {
        view: {
          results: [],
          fields: [],
        },
        loading: false,
        totalPages: 1,
        view_title: 'A Vue View'
      };
    },
};
