

let vue_views_test_page_1 = {
    template: '<div>\
    <h3>{{ view_title }} - Definido en vue views default table.</h3>\
    <table class="admin-dblog-ui">\
      <thead>\
        <tr>\
          <th v-for="field in view.fields">\
                {{ field }}\
          </th>\
        </tr>\
      </thead>\
        <transition name="dblog-ui">\
          <tbody :class="{\'dblog-ui-data-loading\' : loading}">\
            <tr v-for="row in view.results" :key="row.id">\
              <td v-for="value in row">\
                {{ value }}\
              </td>\
            </tr>\
            <tr v-if="!view.results.length" class="dblog-ui-placeholder">\
              <td colspan="6" v-if="loading">Loading...</td>\
              <td colspan="6" v-else>No log messages available.</td>\
            </tr>\
          </tbody>\
        </transition>\
      </table>\
    </div>',
    data: function () {
      return {
        view: {
          results: [],
          fields: [],
        },
        loading: false,
        totalPages: 1,
        view_title: 'A Vue View'
      };
    },
};
